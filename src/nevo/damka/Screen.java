package nevo.damka;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Screen extends JFrame {

	private static final long serialVersionUID = 1L;

	JButton[][] buttons;

	private GridCallBack listener;

	
	public Screen() {
		super();
		setLayout(new GridLayout(8,8));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		buttons = new JButton[8][8];
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				buttons[i][j] = new JButton(i + "," + j);
				add(buttons[i][j]);
				buttons[i][j].addActionListener(new CustomActionListener(i, j));
				setColor(i, j, 0);
			}
		}
		
		setSize(600,600);
		setResizable(false);
		
	}
	
	class CustomActionListener implements ActionListener {

		private int y;
		private int x;

		CustomActionListener(int x,int y){
			this.x = x;
			this.y = y;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(listener!=null)
				listener.onClick(this.x, this.y);
		}
		
	}
	
	void setColor(final int x,final int y,int color){
		if(color == 0)
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					buttons[x][y].setBackground(Color.BLACK);
				}
			});
		else if(color == 1)
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					buttons[x][y].setBackground(Color.RED);
				}
			});
		else if(color == 2)
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					buttons[x][y].setBackground(Color.GREEN);
				}
			});
		else if(color == 3)
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					buttons[x][y].setBackground(Color.YELLOW);
				}
			});
		else
			throw new RuntimeException();
	}
	
	public interface GridCallBack {
		void onClick(int x,int y);
	}
	
	void setClickCallback(GridCallBack l){
		listener = l;
	}
	
}
