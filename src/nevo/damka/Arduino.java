package nevo.damka;

import nevo.damka.Screen.GridCallBack;

public class Arduino {
	
	public static final int BLACK = 0;
	public static final int RED = 1;
	public static final int GREEN = 2;
	
	
	Screen screen;
	
	public Arduino(Screen s) {
		screen = s;
		touchArry = new int[2];
		didTouch = false;
		screen.setClickCallback(new GridCallBack() {
			@Override
			public void onClick(int x, int y) {
				synchronized (Arduino.this) {
					didTouch = true;
					touchArry[0] = x;
					touchArry[1] = y;
				}
			}
		});
		commit();
	}
	
	int touchArry[];
	boolean didTouch;
	
	int detectionClick(){
		synchronized (this) {
			int toReturn =  didTouch ? 1 : 0;
			didTouch = false;
			return toReturn;
		}
	}
	
	void setLed(int x,int y,int color){ 
	  if( color == BLACK ){
	      //bitWrite(ledStateRed[y], x, 0);  
	      ledStateRed[y] &= ~(1<<x); 
	      //bitWrite(ledStateGreen[y], x, 0);      
	      ledStateGreen[y] &= ~(1<<x); 
	    } 
	  else if( color == RED ) {
	      //bitWrite(ledStateRed[y], x, 1); // set bit at x
	      ledStateRed[y] |= 1<<x; 
	    }
	  else if( color == GREEN ) {
	     //bitWrite(ledStateGreen[y], x, 1); // set bit at x
		  ledStateGreen[y] |= 1<<x; 
	  }
	}
	
	void commit(){
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				int redbit = ledStateRed[y] & 1<<x;
				int greenbit = ledStateGreen[y] & 1<<x;
				if(redbit != 0 && greenbit != 0)
					screen.setColor(x, y, 3);
				else if (redbit != 0)
					screen.setColor(x, y, RED);
				else if (greenbit != 0)
					screen.setColor(x, y, GREEN);
				else
					screen.setColor(x, y, BLACK);
			}
		}
	}
	
	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////
	////////////////// JAVA STUFF ABOVE ///////////////////////////////
	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////
	
	int colorTurn = GREEN;
	
	int ledStateRed[] = { 0xAA, 0x55, 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00 } ;
	int ledStateGreen[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x55, 0xAA, 0x55 } ;

	boolean isSelected = false;
	int selectedToolX,selectedToolY;
	
	long lastFlashInvert = 0;
	
	void loop() {

		int x = detectionClick();

		if (x == 1) {
			//toolMovement(touchArry[1], touchArry[0], colorTurn);
			screenClick(touchArry[1], touchArry[0]);
		}
		
		// flashing selected
		if(isSelected){
			if( System.currentTimeMillis() - lastFlashInvert > 200 ){
				lastFlashInvert = System.currentTimeMillis();
				if(colorTurn == GREEN){
					if( tileState(selectedToolY, selectedToolX) == BLACK){
						setLed(selectedToolX, selectedToolY, GREEN);
						commit();
					} else {
						setLed(selectedToolX, selectedToolY, BLACK);
						commit();
					}
				} else if(colorTurn == RED) {
					if( tileState(selectedToolY, selectedToolX) == BLACK){
						setLed(selectedToolX, selectedToolY, RED);
						commit();
					} else {
						setLed(selectedToolX, selectedToolY, BLACK);
						commit();
					}
				} else throw new RuntimeException("reachable?"); // java stuf, delete me in c
			}
		}
			
	}
	
	void screenClick(int y, int x) {
		if( cheakTile(y, x) ){ // we do nothing with illegal tails
			if(colorTurn == GREEN){
				if(isSelected){
					if(selectedToolX == x && selectedToolY == y){
						// pressing on selected item when it sellected, should unselect it
						isSelected = false;
						// make sure it light
						setLed(selectedToolX, selectedToolY, GREEN);
						commit();
					} else {
						// green moves to left y decremented
						if( tileState(y, x) == BLACK && // new location is empty
								selectedToolY - 1 == y && // it is one cell to the left
								(selectedToolX + 1 == x || selectedToolX - 1 == x) // it is on cell up or down
							){
							// light the selected on off
							isSelected = false;
							setLed(selectedToolX, selectedToolY, BLACK);
							// light the new location
							setLed(x, y, GREEN);
							commit();
							colorTurn = RED;
						}
						// eatting up
						else if (
								tileState(y, x) == BLACK && // new location is empty
								selectedToolY - 2 == y && // it is two cell to the left
								selectedToolX - 2 == x && // it is two cell to the top
								tileState(y + 1, x + 1) == RED // the cell in bettwen is red
								){
							isSelected = false;
							setLed(selectedToolX, selectedToolY, BLACK);
							setLed(x, y, GREEN); // set new location
							setLed(x + 1, y + 1, BLACK); // eat
							commit(); 
							colorTurn = RED;
						} 
						// eatting down
						else if (
								tileState(y, x) == BLACK && // new location is empty
								selectedToolY - 2 == y && // it is two cell to the left
								selectedToolX + 2 == x && // it is two cell to the top
								tileState(y + 1, x - 1) == RED // the cell in bettwen is red
								){
							isSelected = false;
							setLed(selectedToolX, selectedToolY, BLACK);
							setLed(x, y, GREEN); // set new location
							setLed(x - 1, y + 1, BLACK); // eat
							commit(); 
							colorTurn = RED;
						} 
					}
				} else {
					if( tileState(y, x) == GREEN ){
						isSelected = true;
						selectedToolX = x;
						selectedToolY = y;
					}
				}
			} else if(colorTurn == RED) {
				if(isSelected){
					if(selectedToolX == x && selectedToolY == y){
						// pressing on selected item when it sellected, should unselect it
						isSelected = false;
						// make sure it light
						setLed(selectedToolX, selectedToolY, RED);
						commit();
					} else {
						// red moves to right y incremnted
						if( tileState(y, x) == BLACK && // new location is empty
								selectedToolY + 1 == y && // it is one cell to the right
								(selectedToolX + 1 == x || selectedToolX - 1 == x) // it is on cell up or down
							){
							// light the selected on off
							isSelected = false;
							setLed(selectedToolX, selectedToolY, BLACK);
							// light the new location
							setLed(x, y, RED);
							commit();
							colorTurn = GREEN;
						}
						// eatting up
						else if (
								tileState(y, x) == BLACK && // new location is empty
								selectedToolY + 2 == y && // it is two cell to the right
								selectedToolX - 2 == x && // it is two cell to the top
								tileState(y - 1, x + 1) == GREEN // the cell in bettwen is green
								){
							isSelected = false;
							setLed(selectedToolX, selectedToolY, BLACK);
							setLed(x, y, RED); // set new location
							setLed(x + 1, y - 1, BLACK); // eat
							commit(); 
							colorTurn = GREEN;
						} 
						// eatting down
						else if (
								tileState(y, x) == BLACK && // new location is empty
								selectedToolY + 2 == y && // it is two cell to the right
								selectedToolX + 2 == x && // it is two cell to the top
								tileState(y - 1, x - 1) == GREEN // the cell in bettwen is green
								){
							isSelected = false;
							setLed(selectedToolX, selectedToolY, BLACK);
							setLed(x, y, RED); // set new location
							setLed(x - 1, y - 1, BLACK); // eat
							commit(); 
							colorTurn = GREEN;
						} 
					}
				} else {
					if( tileState(y, x) == RED ){
						isSelected = true;
						selectedToolX = x;
						selectedToolY = y;
					}
				}
			}
		}
	}

	int tileState( int row, int ledNum ){
	    //boolean Re = bitRead( ledStateRed[row], ledNum );
	    boolean Re = ( ledStateRed[row] & 1<<ledNum ) != 0;
	    //boolean Gr = bitRead( ledStateGreen[row], ledNum );
	    boolean Gr = ( ledStateGreen[row] & 1<<ledNum ) != 0;
	    
	    if ( Gr == false && Re == false ) return BLACK;
	    else if ( Gr == false && Re == true ) return RED;
	    else if ( Gr == true && Re == false ) return GREEN;
	    
	    else throw new RuntimeException("what am i doing here???"); // java stuf, delete me in c
	}
	  
	 boolean cheakTile( int row, int ledNum ){
	     if  (   (  ( row%2 == 0) && (ledNum%2 == 1) ) || ( ( row%2 == 1) && (ledNum%2 == 0)  )    )
	        return true;   
	     else 
			return false;         
	 }
	
	
	
}
