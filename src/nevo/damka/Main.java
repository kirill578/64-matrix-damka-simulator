package nevo.damka;


public class Main {

	public static void main(String[] args) {
		
		Screen s = new Screen();
		final Arduino arduino = new Arduino(s);
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
					arduino.loop();
				}
			}
		}).start();
		
	}
	
}
